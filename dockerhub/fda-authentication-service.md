
# How to use this image

```console  
$ docker run -p 8443:8443 -p 8080:8080 dbrepo/authentication-service:1.3
```  

## Customization

Change the default behavior by setting the following environment variables to different values:

* `METADATA_USERNAME`, default: `root`

  The [metadata database](https://hub.docker.com/r/dbrepo/metadata-db) database username.

* `METADATA_PASSWORD`, default: `dbrepo`

  The [metadata database](https://hub.docker.com/r/dbrepo/metadata-db) database password.

* `KEYCLOAK_ADMIN`, default: `fda`

  The admin user account username. It is **important** to change this value for public deployments.

* `KEYCLOAK_ADMIN_PASSWORD`, default: `fda`

  The admin user account password. It is **important** to change this value for public deployments.

* `KEYCLOAK_IMPORT`, default: `/opt/keycloak/data/import/dbrepo-realm.json`

  The default realm "dbrepo" that is pre-configured with the roles from the backend services. Only change this value  
  if you want to not use the pre-configured realm and want to use your own. In this case, set the `KEYCLOAK_IMPORT=`  
  to empty.

## Keycloak Endpoints

* **Admin Console**:

  https://localhost:8443/admin/master/console/
