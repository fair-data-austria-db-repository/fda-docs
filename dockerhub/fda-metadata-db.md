# How to use this image

```console
$ docker run -v ./metadata-db-data:/var/lib/mysql -p 3306:3306 dbrepo/metadata-db:1.3
```

## Customization

Change the default behavior by setting the following environment variables to different values:

* `METADATA_DB`, default: fda

  The [metadata database](https://hub.docker.com/r/dbrepo/metadata-db) database name.

* `METADATA_USERNAME`, default: postgres

  The [metadata database](https://hub.docker.com/r/dbrepo/metadata-db) database username.

* `METADATA_PASSWORD`, default: postgres

  The [metadata database](https://hub.docker.com/r/dbrepo/metadata-db) database password.

## Actuator Endpoints

* **Prometheus Actuator**:

  http://localhost:9100/actuator/prometheus

## Other Endpoints

* **JDBC Endpoint**:

  `jdbc:mariadb://localhost:3306/fda`
