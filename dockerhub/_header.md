# Quick Reference

* **Maintained by**:

  [the DBRepo Maintainers](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services/-/graphs/master)

* **Where to get help**:

  [the official documentation](https://www.ifs.tuwien.ac.at/infrastructures/dbrepo/),
  [ICoDSE'22 paper](https://doi.org/10.1109/ICoDSE56892.2022.9971958),
  [IJDC'22 paper](https://doi.org/10.2218/ijdc.v17i1.825),
  [iPRES'21 paper](https://doi.org/10.17605/OSF.IO/B7NX5)

# Supported tags

* [`1.3`](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services/-/blob/master/fda-REPOSITORY/Dockerfile/)
* [`latest`](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services/-/blob/dev/fda-REPOSITORY/Dockerfile/)

# Non-supported tags

* `1.2`
* `1.1`

# Quick reference (cont.)

* **Where to file issues**:

  Send us an [email](https://tiss.tuwien.ac.at/person/287722.html)

* **Supported architectures:**

  `amd64`

* **Source of this description:**

  [docs repo's `dockerhub/` directory](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-docs/-/tree/master/dockerhub)
  ([history](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-docs/-/commits/master/dockerhub))

* **Verify image signature**

  [`mweise.pub`](https://www.ifs.tuwien.ac.at/infrastructures/dbrepo/docker/mweise.pub)

# What is DBRepo?

We present a database repository system that allows researchers to ingest data into a central, versioned repository
through common interfaces, provides efficient access to arbitrary subsets of data even when the underlying data store is
evolving, allows reproducing of query results and supports findable-, accessible-, interoperable- and reusable (FAIR) data.

