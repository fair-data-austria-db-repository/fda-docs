# How to use this image

```console
$ docker run -p 9099:9099 dbrepo/metadata-service:1.3
```

## Customization

Change the default behavior by setting the following environment variables to different values:

* `METADATA_DB`, default: `fda`

  The [metadata database](https://hub.docker.com/r/dbrepo/metadata-db) database name.

* `METADATA_USERNAME`, default: `root`

  The [metadata database](https://hub.docker.com/r/dbrepo/metadata-db) database username.

* `METADATA_PASSWORD`, default: `mariadb`

  The [metadata database](https://hub.docker.com/r/dbrepo/metadata-db) database password.

* `GATEWAY_ENDPOINT`, default: `http://gateway-service`

  The gateway endpoint.

* `PID_BASE`, default: `http://localhost/pid/`

  The base redirect endpoint for persistent identifiers in the OAI-PMH protocol.

* `REPOSITORY_NAME`, default: `Example Repository`

  The name of the repository for the OAI-PMH protocol.

* `BASE_URL`, default: `http://localhost`

  The base url of the repository for the OAI-PMH protocol.

* `ADMIN_EMAIL`, default: `noreply@localhost`

  The administrator contact e-mail for the OAI-PMH protocol.

* `EARLIEST_DATESTAMP`, default: `2022-09-17T18:23:00Z`

  The first date and time where the repository was providing information to the public.

* `DELETED_RECORD`, default: `persistent`

  Information for the OAI-PMH protocol on the deletion policy of records.

* `GRANULARITY`, default: `YYYY-MM-DDThh:mm:ssZ`

  Denomination of the record granularity for the OAI-PMH protocol.

* `LOG_LEVEL`, default: `debug`

  The minimum logging level for the service.

## Swagger Endpoints

* **OpenAPI**:

  http://localhost:9099/swagger-ui/index.html

* **OpenAPI `.json`**:
  
  http://localhost:9099/v3/api-docs/

* **OpenAPI `.yaml`**:

  http://localhost:9099/v3/api-docs.yaml
