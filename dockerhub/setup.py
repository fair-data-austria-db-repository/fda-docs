from distutils.core import setup

setup(name='dockerhub-client',
      version='1.0',
      description='Dockerhub Maintenance Client',
      author='Martin Weise',
      author_email='martin.weise@tuwien.ac.at',
      url='dbrepo.ifs.tuwien.ac.at',
      packages=['dockerhub-client'],
      )
