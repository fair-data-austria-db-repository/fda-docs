# How to use this image

```console
$ docker run -v /var/run/docker.sock:/var/run/docker.sock -v /tmp:/tmp -p 5000:5000 dbrepo/analyse-service:1.3
```

## Customization

Change the default behavior by setting the following environment variables to different values:

* `FLASK_APP`, default: `app.py`

  The file that represents a flask app in the container.

* `FLASK_RUN_HOST`, default: `0.0.0.0`

  The bind address for the flask server.

* `PORT_APP`, default: `5000`

  The bind port for the flask server.

* `FLASK_ENV`, default: `production`

  The flask environment mode.

* `HOSTNAME`, default: `analyse-service`

  The name that is used at the [discovery service](https://hub.docker.com/r/dbrepo/discovery-service) to register the
  Analyse Service.

## Swagger Endpoints

* **OpenAPI**:

  http://localhost:5000/swagger-ui/

* **OpenAPI `.json`**:

  http://localhost:5000/api-analyze.json
