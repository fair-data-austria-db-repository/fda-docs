# How to use this image

```console
$ docker run -v /tmp:/tmp -p 3000:3000 dbrepo/ui:1.3
```

## Customization

Change the default behavior by setting the following environment variables to different values:

* `NODE_ENV`, default: `production`

  The environment for node.js.

* `HOST`, default: `0.0.0.0`

  The interface the frontend should bind to and be available at.

* `API`, default: `http://:80`

  The default API endpoint.

* `BASE_URL`, default: `http://:80`

  The base URL for the frontend.

* `BROKER_USERNAME`, default: `fda`

  The [broker service](https://hub.docker.com/r/dbrepo/broker-service) username of a user that has `administrator` tags
  associated to the account.

* `BROKER_PASSWORD`, default: `fda`

  The [broker service](https://hub.docker.com/r/dbrepo/broker-service) password of a user that has `administrator` tags
  associated to the account.

* `SEARCH`, default: `http://search-service:9200`

  The endpoint of the Search Service.

* `SHARED_FILESYSTEM`, default: `/tmp`

  The folder that has a shared filesystem among services.

* `LOGO`, default: `/logo.png`

  The file that is used to display the logo on the left navigation drawer. Change this to your organization's logo by
  mounting it with the `-v` flag, e.g. `docker run -v /path/to/your_logo.png:/logo.png ...`.

* `ELASTIC_USERNAME`, default `elastic`

  The username of the user that can create the search indexes and update them in the search database.

* `ELASTIC_PASSWORD`, default `elastic`

  The password of the user that can create the search indexes and update them in the search database.

* `VERSION`, default: `latest`

  The version number to display on the left navigation drawer.

* `TITLE`, default: `Database Repository`

  The title of the repository.

* `ICON`, default: `/favicon.ico`

  Path to the logo displayed on the left navigation drawer. Change this to your organization's logo by mounting it
  with the `-v` flag, e.g. `docker run -v /path/to/your_logo.ico:/favicon.ico ...`.

* `CLIENT_ID`, default: `dbrepo-client`

  The client id of the [authentication service](https://hub.docker.com/r/dbrepo/authentication-service).

* `DBREPO_CLIENT_SECRET`, default: `MUwRc7yfXSJwX8AdRMWaQC3Nep1VjwgG`

  The client secret of the client with the above client id. This value needs to be changed by regenerating the client
  secret in the [authentication service](https://hub.docker.com/r/dbrepo/authentication-service):

  *Realm dbrepo > Clients > dbrepo-client > Credentials > Client Secret > Regenerate*

* `GIT_HASH`, default: `deadbeef`

  The automatically inserted git hash of the latest commit.

## Graphical Endpoints

* **FAIR Portal**:

  http://localhost:3000/

## Middleware Endpoints

* **HTTP API**:

  http://localhost:3000/api/
