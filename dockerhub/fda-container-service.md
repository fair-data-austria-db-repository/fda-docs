# How to use this image

```console
$ docker run -p 9091:9091 -v /var/run/docker.sock:/var/run/docker.sock dbrepo/container-service:1.3
```

## Customization

Change the default behavior by setting the following environment variables to different values:

* `METADATA_DB`, default: `fda`

  The [Metadata Database](https://hub.docker.com/r/dbrepo/metadata-db) database name.

* `METADATA_USERNAME`, default: `root`

  The [Metadata Database](https://hub.docker.com/r/dbrepo/metadata-db) database username.

* `METADATA_PASSWORD`, default: `mariadb`

  The [Metadata Database](https://hub.docker.com/r/dbrepo/metadata-db) database password.

* `BROKER_USERNAME`, default: `fda`

  The [Broker Service](https://hub.docker.com/r/dbrepo/broker-service) username of a user that has `administrator` tags
  associated to the account.

* `BROKER_PASSWORD`, default: `fda`

  The [Broker Service](https://hub.docker.com/r/dbrepo/broker-service) password of a user that has `administrator` tags
  associated to the account.

* `SHARED_FILESYSTEM`, default: `/tmp`

  The folder that has a shared filesystem among services.

* `USER_NETWORK`, default: `userdb`

  The Docker network that user-generated databases are connected to when created.

* `LOG_LEVEL`, default: `debug`

  The minimum logging level for the service.

* `CLIENT_ID`, default: `dbrepo-client`

  The client id of the [authentication service](https://hub.docker.com/r/dbrepo/authentication-service).

* `DBREPO_CLIENT_SECRET`, default: `MUwRc7yfXSJwX8AdRMWaQC3Nep1VjwgG`

  The client secret of the client with the above client id. This value needs to be changed by regenerating the client
  secret in the [authentication service](https://hub.docker.com/r/dbrepo/authentication-service):

  *Realm dbrepo > Clients > dbrepo-client > Credentials > Client Secret > Regenerate*

* `JWT_ISSUER`, default: `http://localhost/realms/dbrepo`

  The address in the `iss` (issuer) field of the JWT token must match this value.

* `JWT_PUBKEY`, default: `MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqqnHQ2BWWW9vDNLRCcxD++xZg/16oqMo/c1l+lcFEjjAIJjJp/HqrPYU/U9GvquGE6PbVFtTzW1KcKawOW+FJNOA3CGo8Q1TFEfz43B8rZpKsFbJKvQGVv1Z4HaKPvLUm7iMm8Hv91cLduuoWx6Q3DPe2vg13GKKEZe7UFghF+0T9u8EKzA/XqQ0OiICmsmYPbwvf9N3bCKsB/Y10EYmZRb8IhCoV9mmO5TxgWgiuNeCTtNCv2ePYqL/U0WvyGFW0reasIK8eg3KrAUj8DpyOgPOVBn3lBGf+3KFSYi+0bwZbJZWqbC/Xlk20Go1YfeJPRIt7ImxD27R/lNjgDO/MwIDAQAB`

  This value needs to be changed to the RS256 public key from the [authentication-service](https://hub.docker.com/r/dbrepo/authentication-service) in the admin console for public deployments under:

  *dbrepo > Realm settings > Keys > Algorithm > RS256 > Public key*

## Actuator Endpoints

* **Info Actuator**:

  http://localhost:9091/actuator/info

* **Health Actuator**:

  http://localhost:9091/actuator/health

* **Prometheus Actuator**:

  http://localhost:9091/actuator/prometheus

## Swagger Endpoints

* **OpenAPI**:

  http://localhost:9091/swagger-ui/index.html

* **OpenAPI `.json`**:
  
  http://localhost:9091/v3/api-docs/

* **OpenAPI `.yaml`**:

  http://localhost:9091/v3/api-docs.yaml
