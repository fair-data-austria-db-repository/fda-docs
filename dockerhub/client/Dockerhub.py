import requests as rq
import os

class Dockerhub:
    """A simple Dockerhub client"""
    baseurl = "https://hub.docker.com"
    username = ""
    registry = "docker.io"
    workpath = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
    headers = {
        "Content-Type": "application/json",
        "Authorization": None
    }

    def __init__(self):
        self.username = os.getenv("DOCKER_USERNAME")
        print("docker username: %s" % self.username)
        response = rq.post(self.baseurl + "/v2/users/login", {
            "username": self.username,
            "password": os.getenv("DOCKER_PASSWORD")
        })
        if response.status_code == 200:
            self.headers["Authorization"] = "Bearer " + response.json()["token"]
        else:
            raise "Failed to authenticate"


    def modify_description(self, repository):
        header = self.__read__(self.workpath + "/_header.md").replace('REPOSITORY', repository)
        footer = self.__read__(self.workpath + "/_footer.md").replace('REPOSITORY', repository)
        body = self.__read__(self.workpath + "/fda-" + repository + ".md")
        url = self.baseurl + "/v2/repositories/dbrepo/" + repository + "/"
        print("dispatch update: %s" % url)
        response = rq.patch(url, headers=self.headers,
            json={
                "description": "Official repository of DBRepo.",
                "full_description": header + "\n\n" + body + "\n\n" + footer,
                "registry": self.registry
            })
        if response.status_code == 200:
            return response.json()
        else:
            print(response)


    def __read__(self, path):
        with open(path, "r") as f:
            return ' '.join([line for line in f.readlines()])