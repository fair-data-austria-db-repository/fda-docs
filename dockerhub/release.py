#!/bin/env python3
from client.Dockerhub import Dockerhub
from dotenv import load_dotenv

load_dotenv()

dockerhub = Dockerhub()

if __name__ == "__main__":
    for component in ["analyse-service", "authentication-service", "broker-service", "container-service",
                      "database-service", "identifier-service", "metadata-db", "metadata-service", "query-service",
                      "table-service", "ui", "semantics-service", "user-service"]:
        response = dockerhub.modify_description(component)
        print(response)
