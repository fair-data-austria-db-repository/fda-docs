#!/bin/bash

TAG=latest
DOCKER_CONTENT_TRUST=1
DOCKER_CONTENT_TRUST_ROOT_PASSPHRASE=
DOCKER_CONTENT_TRUST_REPOSITORY_PASSPHRASE=

# generate key
#docker trust key generate mweise

# trust
docker trust signer add --key mweise.pub mweise dbrepo/analyse-service
docker trust signer add --key mweise.pub mweise dbrepo/broker-service
docker trust signer add --key mweise.pub mweise dbrepo/container-service
docker trust signer add --key mweise.pub mweise dbrepo/database-service
docker trust signer add --key mweise.pub mweise dbrepo/identifier-service
docker trust signer add --key mweise.pub mweise dbrepo/metadata-db
docker trust signer add --key mweise.pub mweise dbrepo/metadata-service
docker trust signer add --key mweise.pub mweise dbrepo/query-service
docker trust signer add --key mweise.pub mweise dbrepo/semantics-service
docker trust signer add --key mweise.pub mweise dbrepo/table-service
docker trust signer add --key mweise.pub mweise dbrepo/ui
docker trust signer add --key mweise.pub mweise dbrepo/user-service

# sign
docker trust sign dbrepo/analyse-service:${TAG}
docker trust sign dbrepo/broker-service:${TAG}
docker trust sign dbrepo/container-service:${TAG}
docker trust sign dbrepo/database-service:${TAG}
docker trust sign dbrepo/identifier-service:${TAG}
docker trust sign dbrepo/metadata-db:${TAG}
docker trust sign dbrepo/metadata-service:${TAG}
docker trust sign dbrepo/query-service:${TAG}
docker trust sign dbrepo/semantics-service:${TAG}
docker trust sign dbrepo/table-service:${TAG}
docker trust sign dbrepo/ui:${TAG}
docker trust sign dbrepo/user-service:${TAG}

# push
docker push dbrepo/analyse-service:${TAG}
docker push dbrepo/broker-service:${TAG}
docker push dbrepo/container-service:${TAG}
docker push dbrepo/database-service:${TAG}
docker push dbrepo/identifier-service:${TAG}
docker push dbrepo/metadata-db:${TAG}
docker push dbrepo/metadata-service:${TAG}
docker push dbrepo/query-service:${TAG}
docker push dbrepo/semantics-service:${TAG}
docker push dbrepo/table-service:${TAG}
docker push dbrepo/ui:${TAG}
docker push dbrepo/user-service:${TAG}
