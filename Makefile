.PHONY: clean docs

TAG ?= latest

all: build

clean:
	rm -rf ./site

deploy-dockerhub-docs:
	sudo pip3 install -r ./requirements.txt
	python3 ./dockerhub/release.py

verify:
	sudo nginx -t

build:
	mkdocs build
	bash ./swagger/generate.sh

build-swagger:
	bash ./swagger/generate.sh

deploy-docs:
	tar czfv ./final.tar.gz ./final
	scp ./final.tar.gz ec-thoas2:final.tar.gz
	ssh ec-thoas2 "rm -rf /system/user/ifs/infrastructures/public_html/dbrepo/*; tar xzfv ./final.tar.gz; rm -f ./final.tar.gz; cp -r ./final/* /system/user/ifs/infrastructures/public_html/dbrepo; rm -rf ./final"
