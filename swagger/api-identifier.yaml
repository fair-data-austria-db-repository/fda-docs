openapi: 3.0.1
info:
  title: Database Repository Identifier Service API
  description: Service that manages the identifiers
  contact:
    name: Prof. Andreas Rauber
    email: andreas.rauber@tuwien.ac.at
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0
  version: 1.2.0
externalDocs:
  description: Sourcecode Documentation
  url: https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services
servers:
- url: http://localhost:9096
  description: Generated server url
- url: https://dbrepo2.tuwien.ac.at
  description: Sandbox
paths:
  /api/pid/{id}:
    put:
      tags:
      - persistence-endpoint
      summary: Update some identifier
      operationId: update
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/IdentifierUpdateDto'
        required: true
      responses:
        "405":
          description: Updating identifier not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "202":
          description: Updated identifier
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IdentifierDto'
        "404":
          description: Identifier or user could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "400":
          description: Identifier data is not valid to the form
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "406":
          description: Updating identifier not allowed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
      security:
      - bearerAuth: []
    delete:
      tags:
      - persistence-endpoint
      summary: Delete some identifier
      operationId: delete
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "404":
          description: Identifier could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "405":
          description: Deleting identifier not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "202":
          description: Deleted identifier
      security:
      - bearerAuth: []
  /api/identifier:
    get:
      tags:
      - identifier-endpoint
      summary: Find identifiers
      operationId: list
      parameters:
      - name: dbid
        in: query
        required: false
        schema:
          type: integer
          format: int64
      - name: qid
        in: query
        required: false
        schema:
          type: integer
          format: int64
      - name: type
        in: query
        required: false
        schema:
          type: string
          enum:
          - database
          - subset
      responses:
        "200":
          description: List identifiers
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/IdentifierDto'
    post:
      tags:
      - identifier-endpoint
      summary: Create identifier
      operationId: create
      parameters:
      - name: Authorization
        in: header
        required: true
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/IdentifierCreateDto'
        required: true
      responses:
        "406":
          description: Creating identifier not allowed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "400":
          description: Identifier form contains invalid request data
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "502":
          description: Query information could not be retrieved
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "201":
          description: Created identifier
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IdentifierDto'
        "403":
          description: Insufficient access rights or authorities
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "405":
          description: Creating identifier not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "409":
          description: Identifier for this resource already exists
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
      security:
      - bearerAuth: []
  /api/pid/{pid}:
    get:
      tags:
      - persistence-endpoint
      summary: Find some identifier
      operationId: find
      parameters:
      - name: pid
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: Accept
        in: header
        required: true
        schema:
          type: string
      responses:
        "200":
          description: Found identifier successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/IdentifierDto'
            text/csv: {}
            text/xml: {}
            text/bibliography: {}
            text/bibliography; style=apa: {}
            text/bibliography; style=ieee: {}
            text/bibliography; style=bibtex: {}
        "404":
          description: Identifier could not be exported from database as the resource
            was not found
          content:
            text/csv:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "400":
          description: "Identifier could not be exported, the requested style is not\
            \ known"
          content:
            text/bibliography:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "502":
          description: Identifier could not exported from database as it is not reachable
          content:
            text/csv:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
components:
  schemas:
    CreatorDto:
      required:
      - firstname
      - id
      - lastname
      type: object
      properties:
        id:
          type: integer
          format: int64
        firstname:
          type: string
          example: Josiah
        lastname:
          type: string
          example: Carberry
        affiliation:
          type: string
          example: Wesleyan University
        orcid:
          type: string
          example: 0000-0002-1825-0097
    IdentifierUpdateDto:
      required:
      - cid
      - creators
      - dbid
      - publication_year
      - title
      - type
      - visibility
      type: object
      properties:
        cid:
          type: integer
          format: int64
        dbid:
          type: integer
          format: int64
        qid:
          type: integer
          format: int64
        doi:
          type: string
          example: 10.1038/nphys1170
        type:
          type: string
          enum:
          - database
          - subset
        title:
          type: string
          example: "Airquality Stephansplatz, Vienna, Austria"
        description:
          type: string
          example: "Air quality reports at Stephansplatz, Vienna"
        visibility:
          type: string
          example: everyone
          enum:
          - everyone
          - self
        publisher:
          type: string
          example: TU Wien
        language:
          type: string
          enum:
          - ab
          - aa
          - af
          - ak
          - sq
          - am
          - ar
          - an
          - hy
          - as
          - av
          - ae
          - ay
          - az
          - bm
          - ba
          - eu
          - be
          - bn
          - bh
          - bi
          - bs
          - br
          - bg
          - my
          - ca
          - km
          - ch
          - ce
          - ny
          - zh
          - cu
          - cv
          - kw
          - co
          - cr
          - hr
          - cs
          - da
          - dv
          - nl
          - dz
          - en
          - eo
          - et
          - ee
          - fo
          - fj
          - fi
          - fr
          - ff
          - gd
          - gl
          - lg
          - ka
          - de
          - ki
          - el
          - kl
          - gn
          - gu
          - ht
          - ha
          - he
          - hz
          - hi
          - ho
          - hu
          - is
          - io
          - ig
          - id
          - ia
          - ie
          - iu
          - ik
          - ga
          - it
          - ja
          - jv
          - kn
          - kr
          - ks
          - kk
          - rw
          - kv
          - kg
          - ko
          - kj
          - ku
          - ky
          - lo
          - la
          - lv
          - lb
          - li
          - ln
          - lt
          - lu
          - mk
          - mg
          - ms
          - ml
          - mt
          - gv
          - mi
          - mr
          - mh
          - ro
          - mn
          - na
          - nv
          - nd
          - ng
          - ne
          - se
          - "no"
          - nb
          - nn
          - ii
          - oc
          - oj
          - or
          - om
          - os
          - pi
          - pa
          - ps
          - fa
          - pl
          - pt
          - qu
          - rm
          - rn
          - ru
          - sm
          - sg
          - sa
          - sc
          - sr
          - sn
          - sd
          - si
          - sk
          - sl
          - so
          - st
          - nr
          - es
          - su
          - sw
          - ss
          - sv
          - tl
          - ty
          - tg
          - ta
          - tt
          - te
          - th
          - bo
          - ti
          - to
          - ts
          - tn
          - tr
          - tk
          - tw
          - ug
          - uk
          - ur
          - uz
          - ve
          - vi
          - vo
          - wa
          - cy
          - fy
          - wo
          - xh
          - yi
          - yo
          - za
          - zu
        license:
          $ref: '#/components/schemas/LicenseDto'
        creators:
          type: array
          items:
            $ref: '#/components/schemas/CreatorDto'
        publication_day:
          type: integer
          format: int32
          example: 15
        publication_month:
          type: integer
          format: int32
          example: 12
        publication_year:
          type: integer
          format: int32
          example: 2022
        related_identifiers:
          type: array
          items:
            $ref: '#/components/schemas/RelatedIdentifierCreateDto'
    LicenseDto:
      required:
      - identifier
      - uri
      type: object
      properties:
        identifier:
          type: string
          example: MIT
        uri:
          type: string
          example: https://opensource.org/licenses/MIT
    RelatedIdentifierCreateDto:
      required:
      - value
      type: object
      properties:
        value:
          type: string
          example: 10.70124/dc4zh-9ce78
        type:
          type: string
          example: DOI
          enum:
          - DOI
          - URL
          - URN
          - ARK
          - arXiv
          - bibcode
          - EAN13
          - EISSN
          - Handle
          - IGSN
          - ISBN
          - ISTC
          - LISSN
          - LSID
          - PMID
          - PURL
          - UPC
          - w3id
        relation:
          type: string
          example: Cites
          enum:
          - IsCitedBy
          - Cites
          - IsSupplementTo
          - IsSupplementedBy
          - IsContinuedBy
          - Continues
          - IsDescribedBy
          - Describes
          - HasMetadata
          - IsMetadataFor
          - HasVersion
          - IsVersionOf
          - IsNewVersionOf
          - IsPreviousVersionOf
          - IsPartOf
          - HasPart
          - IsPublishedIn
          - IsReferencedBy
          - References
          - IsDocumentedBy
          - Documents
          - IsCompiledBy
          - Compiles
          - IsVariantFormOf
          - IsOriginalFormOf
          - IsIdenticalTo
          - IsReviewedBy
          - Reviews
          - IsDerivedFrom
          - IsSourceOf
          - IsRequiredBy
          - Requires
          - IsObsoletedBy
          - Obsoletes
    ApiErrorDto:
      required:
      - code
      - message
      - status
      type: object
      properties:
        status:
          type: string
          example: STATUS
          enum:
          - 100 CONTINUE
          - 101 SWITCHING_PROTOCOLS
          - 102 PROCESSING
          - 103 EARLY_HINTS
          - 103 CHECKPOINT
          - 200 OK
          - 201 CREATED
          - 202 ACCEPTED
          - 203 NON_AUTHORITATIVE_INFORMATION
          - 204 NO_CONTENT
          - 205 RESET_CONTENT
          - 206 PARTIAL_CONTENT
          - 207 MULTI_STATUS
          - 208 ALREADY_REPORTED
          - 226 IM_USED
          - 300 MULTIPLE_CHOICES
          - 301 MOVED_PERMANENTLY
          - 302 FOUND
          - 302 MOVED_TEMPORARILY
          - 303 SEE_OTHER
          - 304 NOT_MODIFIED
          - 305 USE_PROXY
          - 307 TEMPORARY_REDIRECT
          - 308 PERMANENT_REDIRECT
          - 400 BAD_REQUEST
          - 401 UNAUTHORIZED
          - 402 PAYMENT_REQUIRED
          - 403 FORBIDDEN
          - 404 NOT_FOUND
          - 405 METHOD_NOT_ALLOWED
          - 406 NOT_ACCEPTABLE
          - 407 PROXY_AUTHENTICATION_REQUIRED
          - 408 REQUEST_TIMEOUT
          - 409 CONFLICT
          - 410 GONE
          - 411 LENGTH_REQUIRED
          - 412 PRECONDITION_FAILED
          - 413 PAYLOAD_TOO_LARGE
          - 413 REQUEST_ENTITY_TOO_LARGE
          - 414 URI_TOO_LONG
          - 414 REQUEST_URI_TOO_LONG
          - 415 UNSUPPORTED_MEDIA_TYPE
          - 416 REQUESTED_RANGE_NOT_SATISFIABLE
          - 417 EXPECTATION_FAILED
          - 418 I_AM_A_TEAPOT
          - 419 INSUFFICIENT_SPACE_ON_RESOURCE
          - 420 METHOD_FAILURE
          - 421 DESTINATION_LOCKED
          - 422 UNPROCESSABLE_ENTITY
          - 423 LOCKED
          - 424 FAILED_DEPENDENCY
          - 425 TOO_EARLY
          - 426 UPGRADE_REQUIRED
          - 428 PRECONDITION_REQUIRED
          - 429 TOO_MANY_REQUESTS
          - 431 REQUEST_HEADER_FIELDS_TOO_LARGE
          - 451 UNAVAILABLE_FOR_LEGAL_REASONS
          - 500 INTERNAL_SERVER_ERROR
          - 501 NOT_IMPLEMENTED
          - 502 BAD_GATEWAY
          - 503 SERVICE_UNAVAILABLE
          - 504 GATEWAY_TIMEOUT
          - 505 HTTP_VERSION_NOT_SUPPORTED
          - 506 VARIANT_ALSO_NEGOTIATES
          - 507 INSUFFICIENT_STORAGE
          - 508 LOOP_DETECTED
          - 509 BANDWIDTH_LIMIT_EXCEEDED
          - 510 NOT_EXTENDED
          - 511 NETWORK_AUTHENTICATION_REQUIRED
        message:
          type: string
          example: Error message
        code:
          type: string
          example: error.service.code
    IdentifierDto:
      required:
      - container id
      - creators
      - database id
      - execution
      - publication_year
      - query
      - query_hash
      - query_normalized
      - result_hash
      - result_number
      - title
      - type
      - visibility
      type: object
      properties:
        id:
          type: integer
          format: int64
        type:
          type: string
          enum:
          - database
          - subset
        title:
          type: string
          example: "Airquality Stephansplatz, Vienna, Austria"
        description:
          type: string
          example: "Air quality reports at Stephansplatz, Vienna"
        query:
          type: string
          example: "SELECT `id`, `value`, `location` FROM `air_quality` WHERE `location`\
            \ = \"09:STEF\""
        execution:
          type: string
          format: date-time
        visibility:
          type: string
          example: everyone
          enum:
          - everyone
          - self
        doi:
          type: string
          example: 10.1038/nphys1170
        publisher:
          type: string
          example: TU Wien
        language:
          type: string
          enum:
          - ab
          - aa
          - af
          - ak
          - sq
          - am
          - ar
          - an
          - hy
          - as
          - av
          - ae
          - ay
          - az
          - bm
          - ba
          - eu
          - be
          - bn
          - bh
          - bi
          - bs
          - br
          - bg
          - my
          - ca
          - km
          - ch
          - ce
          - ny
          - zh
          - cu
          - cv
          - kw
          - co
          - cr
          - hr
          - cs
          - da
          - dv
          - nl
          - dz
          - en
          - eo
          - et
          - ee
          - fo
          - fj
          - fi
          - fr
          - ff
          - gd
          - gl
          - lg
          - ka
          - de
          - ki
          - el
          - kl
          - gn
          - gu
          - ht
          - ha
          - he
          - hz
          - hi
          - ho
          - hu
          - is
          - io
          - ig
          - id
          - ia
          - ie
          - iu
          - ik
          - ga
          - it
          - ja
          - jv
          - kn
          - kr
          - ks
          - kk
          - rw
          - kv
          - kg
          - ko
          - kj
          - ku
          - ky
          - lo
          - la
          - lv
          - lb
          - li
          - ln
          - lt
          - lu
          - mk
          - mg
          - ms
          - ml
          - mt
          - gv
          - mi
          - mr
          - mh
          - ro
          - mn
          - na
          - nv
          - nd
          - ng
          - ne
          - se
          - "no"
          - nb
          - nn
          - ii
          - oc
          - oj
          - or
          - om
          - os
          - pi
          - pa
          - ps
          - fa
          - pl
          - pt
          - qu
          - rm
          - rn
          - ru
          - sm
          - sg
          - sa
          - sc
          - sr
          - sn
          - sd
          - si
          - sk
          - sl
          - so
          - st
          - nr
          - es
          - su
          - sw
          - ss
          - sv
          - tl
          - ty
          - tg
          - ta
          - tt
          - te
          - th
          - bo
          - ti
          - to
          - ts
          - tn
          - tr
          - tk
          - tw
          - ug
          - uk
          - ur
          - uz
          - ve
          - vi
          - vo
          - wa
          - cy
          - fy
          - wo
          - xh
          - yi
          - yo
          - za
          - zu
        license:
          $ref: '#/components/schemas/LicenseDto'
        creators:
          type: array
          items:
            $ref: '#/components/schemas/CreatorDto'
        created:
          type: string
          format: date-time
        container id:
          type: integer
          format: int64
          example: 1
        database id:
          type: integer
          format: int64
          example: 1
        query id:
          type: integer
          format: int64
          example: 1
        query_normalized:
          type: string
          example: "SELECT `id`, `value`, `location` FROM `air_quality` WHERE `location`\
            \ = \"09:STEF\""
        related:
          type: array
          items:
            $ref: '#/components/schemas/RelatedIdentifierDto'
        query_hash:
          type: string
          description: query hash in sha512
        result_hash:
          type: string
        result_number:
          type: integer
          format: int64
          example: 1
        publication_day:
          type: integer
          format: int32
          example: 15
        publication_month:
          type: integer
          format: int32
          example: 12
        publication_year:
          type: integer
          format: int32
          example: 2022
        last_modified:
          type: string
          format: date-time
    RelatedIdentifierDto:
      required:
      - created
      - id
      - value
      type: object
      properties:
        id:
          type: integer
          format: int64
        value:
          type: string
          example: 10.70124/dc4zh-9ce78
        type:
          type: string
          example: DOI
          enum:
          - DOI
          - URL
          - URN
          - ARK
          - arXiv
          - bibcode
          - EAN13
          - EISSN
          - Handle
          - IGSN
          - ISBN
          - ISTC
          - LISSN
          - LSID
          - PMID
          - PURL
          - UPC
          - w3id
        relation:
          type: string
          example: Cites
          enum:
          - IsCitedBy
          - Cites
          - IsSupplementTo
          - IsSupplementedBy
          - IsContinuedBy
          - Continues
          - IsDescribedBy
          - Describes
          - HasMetadata
          - IsMetadataFor
          - HasVersion
          - IsVersionOf
          - IsNewVersionOf
          - IsPreviousVersionOf
          - IsPartOf
          - HasPart
          - IsPublishedIn
          - IsReferencedBy
          - References
          - IsDocumentedBy
          - Documents
          - IsCompiledBy
          - Compiles
          - IsVariantFormOf
          - IsOriginalFormOf
          - IsIdenticalTo
          - IsReviewedBy
          - Reviews
          - IsDerivedFrom
          - IsSourceOf
          - IsRequiredBy
          - Requires
          - IsObsoletedBy
          - Obsoletes
        created:
          type: string
          format: date-time
        last_modified:
          type: string
          format: date-time
    CreatorCreateDto:
      required:
      - firstname
      - lastname
      type: object
      properties:
        firstname:
          type: string
          example: Josiah
        lastname:
          type: string
          example: Carberry
        affiliation:
          type: string
          example: Wesleyan University
        orcid:
          type: string
          example: 0000-0002-1825-0097
    IdentifierCreateDto:
      required:
      - cid
      - creators
      - dbid
      - publication_year
      - title
      - type
      - visibility
      type: object
      properties:
        cid:
          type: integer
          format: int64
        dbid:
          type: integer
          format: int64
        qid:
          type: integer
          format: int64
        type:
          type: string
          enum:
          - database
          - subset
        title:
          type: string
          example: "Airquality Stephansplatz, Vienna, Austria"
        description:
          type: string
          example: "Air quality reports at Stephansplatz, Vienna"
        visibility:
          type: string
          example: everyone
          enum:
          - everyone
          - self
        publisher:
          type: string
          example: TU Wien
        language:
          type: string
          enum:
          - ab
          - aa
          - af
          - ak
          - sq
          - am
          - ar
          - an
          - hy
          - as
          - av
          - ae
          - ay
          - az
          - bm
          - ba
          - eu
          - be
          - bn
          - bh
          - bi
          - bs
          - br
          - bg
          - my
          - ca
          - km
          - ch
          - ce
          - ny
          - zh
          - cu
          - cv
          - kw
          - co
          - cr
          - hr
          - cs
          - da
          - dv
          - nl
          - dz
          - en
          - eo
          - et
          - ee
          - fo
          - fj
          - fi
          - fr
          - ff
          - gd
          - gl
          - lg
          - ka
          - de
          - ki
          - el
          - kl
          - gn
          - gu
          - ht
          - ha
          - he
          - hz
          - hi
          - ho
          - hu
          - is
          - io
          - ig
          - id
          - ia
          - ie
          - iu
          - ik
          - ga
          - it
          - ja
          - jv
          - kn
          - kr
          - ks
          - kk
          - rw
          - kv
          - kg
          - ko
          - kj
          - ku
          - ky
          - lo
          - la
          - lv
          - lb
          - li
          - ln
          - lt
          - lu
          - mk
          - mg
          - ms
          - ml
          - mt
          - gv
          - mi
          - mr
          - mh
          - ro
          - mn
          - na
          - nv
          - nd
          - ng
          - ne
          - se
          - "no"
          - nb
          - nn
          - ii
          - oc
          - oj
          - or
          - om
          - os
          - pi
          - pa
          - ps
          - fa
          - pl
          - pt
          - qu
          - rm
          - rn
          - ru
          - sm
          - sg
          - sa
          - sc
          - sr
          - sn
          - sd
          - si
          - sk
          - sl
          - so
          - st
          - nr
          - es
          - su
          - sw
          - ss
          - sv
          - tl
          - ty
          - tg
          - ta
          - tt
          - te
          - th
          - bo
          - ti
          - to
          - ts
          - tn
          - tr
          - tk
          - tw
          - ug
          - uk
          - ur
          - uz
          - ve
          - vi
          - vo
          - wa
          - cy
          - fy
          - wo
          - xh
          - yi
          - yo
          - za
          - zu
        license:
          $ref: '#/components/schemas/LicenseDto'
        creators:
          type: array
          items:
            $ref: '#/components/schemas/CreatorCreateDto'
        publication_day:
          type: integer
          format: int32
          example: 15
        publication_month:
          type: integer
          format: int32
          example: 12
        publication_year:
          type: integer
          format: int32
          example: 2022
        related_identifiers:
          type: array
          items:
            $ref: '#/components/schemas/RelatedIdentifierCreateDto'
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
