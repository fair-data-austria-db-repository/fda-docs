openapi: 3.0.1
info:
  title: Database Repository Database Service API
  description: Service that manages the databases
  contact:
    name: Prof. Andreas Rauber
    email: andreas.rauber@tuwien.ac.at
  license:
    name: Apache 2.0
    url: https://www.apache.org/licenses/LICENSE-2.0
  version: 1.2.0
externalDocs:
  description: Sourcecode Documentation
  url: https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services
servers:
- url: http://localhost:9092
  description: Generated server url
- url: https://dbrepo2.tuwien.ac.at
  description: Sandbox
paths:
  /api/container/{id}/database/{databaseId}/visibility:
    put:
      tags:
      - database-endpoint
      summary: Update database
      operationId: visibility
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: databaseId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DatabaseModifyVisibilityDto'
        required: true
      responses:
        "404":
          description: Database or user could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "202":
          description: Visibility modified successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DatabaseDto'
        "405":
          description: Visibility modification is not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
      security:
      - bearerAuth: []
  /api/container/{id}/database/{databaseId}/transfer:
    put:
      tags:
      - database-endpoint
      summary: Transfer database
      operationId: transfer
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: databaseId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DatabaseTransferDto'
        required: true
      responses:
        "202":
          description: Transfer of ownership was successful
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DatabaseDto'
        "404":
          description: Database or user could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "405":
          description: Transfer of ownership is not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
      security:
      - bearerAuth: []
  /api/container/{id}/database/{databaseId}/access/{username}:
    put:
      tags:
      - access-endpoint
      summary: Modify access to some database
      operationId: update
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: databaseId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: username
        in: path
        required: true
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DatabaseModifyAccessDto'
        required: true
      responses:
        "404":
          description: Database or user not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "400":
          description: Modify access query or database connection is malformed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "202":
          description: Modify access succeeded
        "403":
          description: Modify access not permitted when no access is granted in the
            first place
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
      security:
      - bearerAuth: []
    delete:
      tags:
      - access-endpoint
      summary: Revoke access to some database
      operationId: revoke
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: databaseId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: username
        in: path
        required: true
        schema:
          type: string
      responses:
        "404":
          description: User with access was not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "400":
          description: Modify access query or database connection is malformed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "405":
          description: Revoke of access not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "403":
          description: Revoke of access not permitted as no access was found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "202":
          description: Revoked access successfully
      security:
      - bearerAuth: []
  /api/container/{id}/database:
    get:
      tags:
      - database-endpoint
      summary: List databases
      operationId: list
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: List of databases
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/DatabaseBriefDto'
    post:
      tags:
      - database-endpoint
      summary: Create database
      operationId: create
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DatabaseCreateDto'
        required: true
      responses:
        "201":
          description: Created a new database
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DatabaseBriefDto'
        "404":
          description: "Container, user or database could not be found"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "406":
          description: Failed to create user at broker service or virtual host could
            not be reached at broker service
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "405":
          description: Database create permission is missing or grant permissions
            at broker service failed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "501":
          description: Container image is not supported
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "503":
          description: Connection to the database failed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "502":
          description: Connection to the container failed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "400":
          description: Database create query is malformed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "409":
          description: Database name already exist or query store could not be created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
      security:
      - bearerAuth: []
  /api/container/{id}/database/{databaseId}/access:
    get:
      tags:
      - access-endpoint
      summary: Check access to some database
      operationId: find
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: databaseId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "403":
          description: No access to this database
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "200":
          description: Found database access
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DatabaseAccessDto'
        "405":
          description: Check access is not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
      security:
      - bearerAuth: []
    post:
      tags:
      - access-endpoint
      summary: Give access to some database
      operationId: create_1
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: databaseId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DatabaseGiveAccessDto'
        required: true
      responses:
        "202":
          description: Granting access succeeded
        "405":
          description: Granting access not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "400":
          description: Granting access query or database connection is malformed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "404":
          description: Database or user not found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
      security:
      - bearerAuth: []
  /api/container/{id}/database/{databaseId}:
    get:
      tags:
      - database-endpoint
      summary: Find some database
      operationId: findById
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: databaseId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "405":
          description: Database information is not permitted
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "404":
          description: Database or container could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "200":
          description: Database found successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DatabaseDto'
      security:
      - bearerAuth: []
    delete:
      tags:
      - database-endpoint
      summary: Delete some database
      operationId: delete
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      - name: databaseId
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "400":
          description: Database delete query is malformed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "404":
          description: Container or database could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "405":
          description: Database delete permission is missing or revoke permissions
            at broker service failed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "501":
          description: Container image is not supported
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "503":
          description: Connection to the database failed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "406":
          description: Failed to delete user at broker service or virtual host could
            not be reached at broker service
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "502":
          description: Connection to the container failed
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiErrorDto'
        "201":
          description: Deleted a database
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DatabaseBriefDto'
      security:
      - bearerAuth: []
  /api/container/{id}/database/license:
    get:
      tags:
      - license-endpoint
      summary: Get all licenses
      operationId: list_1
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: integer
          format: int64
      responses:
        "200":
          description: List of licenses
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/LicenseDto'
components:
  schemas:
    DatabaseModifyVisibilityDto:
      required:
      - is_public
      type: object
      properties:
        is_public:
          type: boolean
          example: true
    ApiErrorDto:
      required:
      - code
      - message
      - status
      type: object
      properties:
        status:
          type: string
          example: STATUS
          enum:
          - 100 CONTINUE
          - 101 SWITCHING_PROTOCOLS
          - 102 PROCESSING
          - 103 EARLY_HINTS
          - 103 CHECKPOINT
          - 200 OK
          - 201 CREATED
          - 202 ACCEPTED
          - 203 NON_AUTHORITATIVE_INFORMATION
          - 204 NO_CONTENT
          - 205 RESET_CONTENT
          - 206 PARTIAL_CONTENT
          - 207 MULTI_STATUS
          - 208 ALREADY_REPORTED
          - 226 IM_USED
          - 300 MULTIPLE_CHOICES
          - 301 MOVED_PERMANENTLY
          - 302 FOUND
          - 302 MOVED_TEMPORARILY
          - 303 SEE_OTHER
          - 304 NOT_MODIFIED
          - 305 USE_PROXY
          - 307 TEMPORARY_REDIRECT
          - 308 PERMANENT_REDIRECT
          - 400 BAD_REQUEST
          - 401 UNAUTHORIZED
          - 402 PAYMENT_REQUIRED
          - 403 FORBIDDEN
          - 404 NOT_FOUND
          - 405 METHOD_NOT_ALLOWED
          - 406 NOT_ACCEPTABLE
          - 407 PROXY_AUTHENTICATION_REQUIRED
          - 408 REQUEST_TIMEOUT
          - 409 CONFLICT
          - 410 GONE
          - 411 LENGTH_REQUIRED
          - 412 PRECONDITION_FAILED
          - 413 PAYLOAD_TOO_LARGE
          - 413 REQUEST_ENTITY_TOO_LARGE
          - 414 URI_TOO_LONG
          - 414 REQUEST_URI_TOO_LONG
          - 415 UNSUPPORTED_MEDIA_TYPE
          - 416 REQUESTED_RANGE_NOT_SATISFIABLE
          - 417 EXPECTATION_FAILED
          - 418 I_AM_A_TEAPOT
          - 419 INSUFFICIENT_SPACE_ON_RESOURCE
          - 420 METHOD_FAILURE
          - 421 DESTINATION_LOCKED
          - 422 UNPROCESSABLE_ENTITY
          - 423 LOCKED
          - 424 FAILED_DEPENDENCY
          - 425 TOO_EARLY
          - 426 UPGRADE_REQUIRED
          - 428 PRECONDITION_REQUIRED
          - 429 TOO_MANY_REQUESTS
          - 431 REQUEST_HEADER_FIELDS_TOO_LARGE
          - 451 UNAVAILABLE_FOR_LEGAL_REASONS
          - 500 INTERNAL_SERVER_ERROR
          - 501 NOT_IMPLEMENTED
          - 502 BAD_GATEWAY
          - 503 SERVICE_UNAVAILABLE
          - 504 GATEWAY_TIMEOUT
          - 505 HTTP_VERSION_NOT_SUPPORTED
          - 506 VARIANT_ALSO_NEGOTIATES
          - 507 INSUFFICIENT_STORAGE
          - 508 LOOP_DETECTED
          - 509 BANDWIDTH_LIMIT_EXCEEDED
          - 510 NOT_EXTENDED
          - 511 NETWORK_AUTHENTICATION_REQUIRED
        message:
          type: string
          example: Error message
        code:
          type: string
          example: error.service.code
    ColumnBriefDto:
      required:
      - column_type
      - database_id
      - id
      - internal_name
      - name
      - table_id
      type: object
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
          example: date
        database_id:
          type: integer
          format: int64
        table_id:
          type: integer
          format: int64
        internal_name:
          type: string
          example: mdb_date
        column_type:
          type: string
          example: date
          enum:
          - enum
          - number
          - decimal
          - string
          - text
          - boolean
          - date
          - timestamp
          - blob
    ContainerDto:
      required:
      - created
      - hash
      - id
      - internal_name
      - name
      - running
      type: object
      properties:
        id:
          type: integer
          format: int64
        hash:
          type: string
          example: f829dd8a884182d0da846f365dee1221fd16610a14c81b8f9f295ff162749e50
        name:
          type: string
          example: Air Quality
        state:
          type: string
          example: running
          enum:
          - created
          - restarting
          - running
          - paused
          - exited
          - dead
        database:
          $ref: '#/components/schemas/DatabaseDto'
        running:
          type: boolean
          example: true
        image:
          $ref: '#/components/schemas/ImageBriefDto'
        port:
          type: integer
          format: int32
        owner:
          $ref: '#/components/schemas/UserBriefDto'
        created:
          type: string
          format: date-time
          example: 2021-03-12T15:26:21.678396092Z
        internal_name:
          type: string
          example: air-quality
        ip_address:
          type: string
    CreatorDto:
      required:
      - firstname
      - id
      - lastname
      type: object
      properties:
        id:
          type: integer
          format: int64
        firstname:
          type: string
          example: Josiah
        lastname:
          type: string
          example: Carberry
        affiliation:
          type: string
          example: Wesleyan University
        orcid:
          type: string
          example: 0000-0002-1825-0097
    DatabaseAccessDto:
      required:
      - type
      - user
      type: object
      properties:
        user:
          $ref: '#/components/schemas/UserDto'
        type:
          type: string
          enum:
          - read
          - write_own
          - write_all
        created:
          type: string
          format: date-time
    DatabaseDto:
      required:
      - creator
      - exchange_name
      - id
      - internal_name
      - name
      - owner
      type: object
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
          example: Air Quality
        identifier:
          $ref: '#/components/schemas/IdentifierDto'
        description:
          type: string
          example: Weather Australia 2009-2021
        tables:
          type: array
          items:
            $ref: '#/components/schemas/TableBriefDto'
        views:
          type: array
          items:
            $ref: '#/components/schemas/ViewBriefDto'
        image:
          $ref: '#/components/schemas/ImageDto'
        container:
          $ref: '#/components/schemas/ContainerDto'
        accesses:
          type: array
          items:
            $ref: '#/components/schemas/DatabaseAccessDto'
        creator:
          $ref: '#/components/schemas/UserBriefDto'
        owner:
          $ref: '#/components/schemas/UserBriefDto'
        created:
          type: string
          format: date-time
        exchange_name:
          type: string
          example: dbrepo/air_quality
        internal_name:
          type: string
          example: weather_australia
        is_public:
          type: boolean
          example: true
    IdentifierDto:
      required:
      - container id
      - creators
      - database id
      - execution
      - publication_year
      - query
      - query_hash
      - query_normalized
      - result_hash
      - result_number
      - title
      - type
      - visibility
      type: object
      properties:
        id:
          type: integer
          format: int64
        type:
          type: string
          enum:
          - database
          - subset
        title:
          type: string
          example: "Airquality Stephansplatz, Vienna, Austria"
        description:
          type: string
          example: "Air quality reports at Stephansplatz, Vienna"
        query:
          type: string
          example: "SELECT `id`, `value`, `location` FROM `air_quality` WHERE `location`\
            \ = \"09:STEF\""
        execution:
          type: string
          format: date-time
        visibility:
          type: string
          example: everyone
          enum:
          - everyone
          - self
        doi:
          type: string
          example: 10.1038/nphys1170
        publisher:
          type: string
          example: TU Wien
        language:
          type: string
          enum:
          - ab
          - aa
          - af
          - ak
          - sq
          - am
          - ar
          - an
          - hy
          - as
          - av
          - ae
          - ay
          - az
          - bm
          - ba
          - eu
          - be
          - bn
          - bh
          - bi
          - bs
          - br
          - bg
          - my
          - ca
          - km
          - ch
          - ce
          - ny
          - zh
          - cu
          - cv
          - kw
          - co
          - cr
          - hr
          - cs
          - da
          - dv
          - nl
          - dz
          - en
          - eo
          - et
          - ee
          - fo
          - fj
          - fi
          - fr
          - ff
          - gd
          - gl
          - lg
          - ka
          - de
          - ki
          - el
          - kl
          - gn
          - gu
          - ht
          - ha
          - he
          - hz
          - hi
          - ho
          - hu
          - is
          - io
          - ig
          - id
          - ia
          - ie
          - iu
          - ik
          - ga
          - it
          - ja
          - jv
          - kn
          - kr
          - ks
          - kk
          - rw
          - kv
          - kg
          - ko
          - kj
          - ku
          - ky
          - lo
          - la
          - lv
          - lb
          - li
          - ln
          - lt
          - lu
          - mk
          - mg
          - ms
          - ml
          - mt
          - gv
          - mi
          - mr
          - mh
          - ro
          - mn
          - na
          - nv
          - nd
          - ng
          - ne
          - se
          - "no"
          - nb
          - nn
          - ii
          - oc
          - oj
          - or
          - om
          - os
          - pi
          - pa
          - ps
          - fa
          - pl
          - pt
          - qu
          - rm
          - rn
          - ru
          - sm
          - sg
          - sa
          - sc
          - sr
          - sn
          - sd
          - si
          - sk
          - sl
          - so
          - st
          - nr
          - es
          - su
          - sw
          - ss
          - sv
          - tl
          - ty
          - tg
          - ta
          - tt
          - te
          - th
          - bo
          - ti
          - to
          - ts
          - tn
          - tr
          - tk
          - tw
          - ug
          - uk
          - ur
          - uz
          - ve
          - vi
          - vo
          - wa
          - cy
          - fy
          - wo
          - xh
          - yi
          - yo
          - za
          - zu
        license:
          $ref: '#/components/schemas/LicenseDto'
        creators:
          type: array
          items:
            $ref: '#/components/schemas/CreatorDto'
        created:
          type: string
          format: date-time
        container id:
          type: integer
          format: int64
          example: 1
        database id:
          type: integer
          format: int64
          example: 1
        query id:
          type: integer
          format: int64
          example: 1
        query_normalized:
          type: string
          example: "SELECT `id`, `value`, `location` FROM `air_quality` WHERE `location`\
            \ = \"09:STEF\""
        related:
          type: array
          items:
            $ref: '#/components/schemas/RelatedIdentifierDto'
        query_hash:
          type: string
          description: query hash in sha512
        result_hash:
          type: string
        result_number:
          type: integer
          format: int64
          example: 1
        publication_day:
          type: integer
          format: int32
          example: 15
        publication_month:
          type: integer
          format: int32
          example: 12
        publication_year:
          type: integer
          format: int32
          example: 2022
        last_modified:
          type: string
          format: date-time
    ImageBriefDto:
      required:
      - id
      - registry
      - repository
      - tag
      type: object
      properties:
        id:
          type: integer
          format: int64
        registry:
          type: string
          example: docker.io/library
        repository:
          type: string
          example: mariadb
        tag:
          type: string
          example: "10.5"
    ImageDateDto:
      required:
      - database_format
      - example
      - has_time
      - id
      - unix_format
      type: object
      properties:
        id:
          type: integer
          format: int64
        example:
          type: string
          example: 30.01.2022
        database_format:
          type: string
          example: '%d.%c.%Y'
        unix_format:
          type: string
          example: dd.MM.YYYY
        has_time:
          type: boolean
          example: false
        created_at:
          type: string
          format: date-time
    ImageDto:
      required:
      - default_port
      - dialect
      - driver_class
      - id
      - jdbc_method
      - registry
      - repository
      - tag
      type: object
      properties:
        id:
          type: integer
          format: int64
        registry:
          type: string
          example: docker.io/library
        repository:
          type: string
          example: mariadb
        tag:
          type: string
          example: "10.5"
        dialect:
          type: string
          example: org.hibernate.dialect.MariaDBDialect
        hash:
          type: string
          example: sha256:c5ec7353d87dfc35067e7bffeb25d6a0d52dad41e8b7357213e3b12d6e7ff78e
        compiled:
          type: string
          format: date-time
          example: 2021-03-12T15:26:21.678396092Z
        size:
          type: integer
          example: 314295447
        driver_class:
          type: string
          example: org.mariadb.jdbc.Driver
        date_formats:
          type: array
          items:
            $ref: '#/components/schemas/ImageDateDto'
        jdbc_method:
          type: string
          example: mariadb
        default_port:
          type: integer
          format: int32
          example: 3306
    LicenseDto:
      required:
      - identifier
      - uri
      type: object
      properties:
        identifier:
          type: string
          example: MIT
        uri:
          type: string
          example: https://opensource.org/licenses/MIT
    RelatedIdentifierDto:
      required:
      - created
      - id
      - value
      type: object
      properties:
        id:
          type: integer
          format: int64
        value:
          type: string
          example: 10.70124/dc4zh-9ce78
        type:
          type: string
          example: DOI
          enum:
          - DOI
          - URL
          - URN
          - ARK
          - arXiv
          - bibcode
          - EAN13
          - EISSN
          - Handle
          - IGSN
          - ISBN
          - ISTC
          - LISSN
          - LSID
          - PMID
          - PURL
          - UPC
          - w3id
        relation:
          type: string
          example: Cites
          enum:
          - IsCitedBy
          - Cites
          - IsSupplementTo
          - IsSupplementedBy
          - IsContinuedBy
          - Continues
          - IsDescribedBy
          - Describes
          - HasMetadata
          - IsMetadataFor
          - HasVersion
          - IsVersionOf
          - IsNewVersionOf
          - IsPreviousVersionOf
          - IsPartOf
          - HasPart
          - IsPublishedIn
          - IsReferencedBy
          - References
          - IsDocumentedBy
          - Documents
          - IsCompiledBy
          - Compiles
          - IsVariantFormOf
          - IsOriginalFormOf
          - IsIdenticalTo
          - IsReviewedBy
          - Reviews
          - IsDerivedFrom
          - IsSourceOf
          - IsRequiredBy
          - Requires
          - IsObsoletedBy
          - Obsoletes
        created:
          type: string
          format: date-time
        last_modified:
          type: string
          format: date-time
    TableBriefDto:
      required:
      - columns
      - description
      - id
      - internal_name
      - name
      - owner
      type: object
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
          example: Air Quality
        description:
          type: string
          example: Air Quality in Austria
        owner:
          $ref: '#/components/schemas/UserBriefDto'
        columns:
          type: array
          items:
            $ref: '#/components/schemas/ColumnBriefDto'
        internal_name:
          type: string
          example: air_quality
    UserAttributeDto:
      type: object
      properties:
        name:
          type: string
          example: theme_dark
        value:
          type: string
          example: "true"
    UserBriefDto:
      required:
      - id
      - username
      type: object
      properties:
        id:
          type: string
          format: uuid
          example: 1ffc7b0e-9aeb-4e8b-b8f1-68f3936155b4
        username:
          type: string
          description: Only contains lowercase characters
          example: jcarberry
        name:
          type: string
          example: Josiah Carberry
        orcid:
          type: string
          example: 0000-0002-1825-0097
        given_name:
          type: string
          example: Josiah
        family_name:
          type: string
          example: Carberry
        email_verified:
          type: boolean
          example: true
    UserDto:
      required:
      - email
      - email_verified
      - id
      - username
      type: object
      properties:
        id:
          type: string
          format: uuid
          example: 1ffc7b0e-9aeb-4e8b-b8f1-68f3936155b4
        username:
          type: string
          description: Only contains lowercase characters
          example: jcarberry
        name:
          type: string
          example: Josiah Carberry
        orcid:
          type: string
          example: 0000-0002-1825-0097
        attributes:
          type: array
          items:
            $ref: '#/components/schemas/UserAttributeDto'
        containers:
          type: array
          items:
            $ref: '#/components/schemas/ContainerDto'
        databases:
          type: array
          items:
            $ref: '#/components/schemas/ContainerDto'
        identifiers:
          type: array
          items:
            $ref: '#/components/schemas/ContainerDto'
        email:
          type: string
          example: jcarberry@brown.edu
        given_name:
          type: string
          example: Josiah
        family_name:
          type: string
          example: Carberry
        email_verified:
          type: boolean
          example: true
    ViewBriefDto:
      required:
      - created
      - creator
      - id
      - internal_name
      - name
      - query
      - vdbid
      type: object
      properties:
        id:
          type: integer
          format: int64
        vdbid:
          type: integer
          format: int64
        name:
          type: string
          example: Air Quality
        query:
          type: string
          example: SELECT `id` FROM `air_quality` ORDER BY `value` DESC
        created:
          type: string
          format: date-time
        creator:
          $ref: '#/components/schemas/UserDto'
        internal_name:
          type: string
          example: air_quality
        is_public:
          type: boolean
          example: true
        initial_view:
          type: boolean
          description: True if it is the default view for the database
          example: true
        last_modified:
          type: string
          format: date-time
    DatabaseTransferDto:
      required:
      - username
      type: object
      properties:
        username:
          type: string
    DatabaseModifyAccessDto:
      required:
      - type
      type: object
      properties:
        type:
          type: string
          enum:
          - read
          - write_own
          - write_all
    DatabaseCreateDto:
      required:
      - is_public
      - name
      type: object
      properties:
        name:
          type: string
          example: Air Quality
        is_public:
          type: boolean
          example: true
    ContainerBriefDto:
      required:
      - creator
      - hash
      - id
      - internal_name
      - name
      - running
      type: object
      properties:
        id:
          type: integer
          format: int64
        hash:
          type: string
          example: f829dd8a884182d0da846f365dee1221fd16610a14c81b8f9f295ff162749e50
        name:
          type: string
          example: Air Quality
        creator:
          $ref: '#/components/schemas/UserBriefDto'
        running:
          type: boolean
          example: true
        database:
          $ref: '#/components/schemas/DatabaseBriefDto'
        created:
          type: string
          format: date-time
        internal_name:
          type: string
          example: air-quality
    CreatorBriefDto:
      required:
      - firstname
      - lastname
      type: object
      properties:
        firstname:
          type: string
          example: Josiah
        lastname:
          type: string
          example: Carberry
        affiliation:
          type: string
          example: Wesleyan University
    DatabaseBriefDto:
      required:
      - id
      - name
      - owner
      type: object
      properties:
        id:
          type: integer
          format: int64
        name:
          type: string
          example: Air Quality
        description:
          type: string
          example: Air Quality in Austria
        identifier:
          $ref: '#/components/schemas/IdentifierBriefDto'
        engine:
          type: string
          example: mariadb:10.5
        owner:
          $ref: '#/components/schemas/UserBriefDto'
        container:
          $ref: '#/components/schemas/ContainerBriefDto'
        creator:
          $ref: '#/components/schemas/UserBriefDto'
        created:
          type: string
          format: date-time
        is_public:
          type: boolean
          example: true
    IdentifierBriefDto:
      required:
      - container id
      - creators
      - database id
      - id
      - publication_year
      - title
      - type
      type: object
      properties:
        id:
          type: integer
          format: int64
        title:
          type: string
          example: "Airquality Stephansplatz, Vienna, Austria"
        type:
          type: string
          enum:
          - database
          - subset
        doi:
          type: string
          example: 10.1038/nphys1170
        publisher:
          type: string
          example: TU Wien
        creators:
          type: array
          items:
            $ref: '#/components/schemas/CreatorBriefDto'
        container id:
          type: integer
          format: int64
          example: 1
        database id:
          type: integer
          format: int64
          example: 1
        query id:
          type: integer
          format: int64
          example: 1
        publication_year:
          type: integer
          format: int32
          example: 2022
    DatabaseGiveAccessDto:
      required:
      - type
      - username
      type: object
      properties:
        username:
          type: string
        type:
          type: string
          enum:
          - read
          - write_own
          - write_all
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
