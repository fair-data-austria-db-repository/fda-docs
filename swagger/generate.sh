#!/bin/bash

declare -A services
services[5000]=analyse
services[9091]=container
services[9092]=database
services[9093]=query
services[9094]=table
services[9096]=identifier
services[9097]=semantics
services[9098]=user
services[9099]=metadata

function retrieve () {
  if [[ "$2" == analyse ]]; then
    echo "... retrieve json api from localhost:$1"
    wget "http://localhost:$1/api-$2.json" -O "./swagger/api-$2.yaml" -q
  else
    echo "... retrieve yaml api from localhost:$1"
    wget "http://localhost:$1/v3/api-docs.yaml" -O "./swagger/api-$2.yaml" -q
  fi
}

function generate () {
  echo "... generate python api"
  java -jar swagger-codegen-cli.jar generate -i "./swagger/api-$1.yaml" -l python -o "./swagger/api-$1" > /dev/null
}

function remove () {
  echo "... removing old python api"
  rm -rf "./swagger/api/api_$1"
}

function copy () {
  echo "... copying python api"
  cp -r "./swagger/api-$1/swagger_client" "./swagger/api/api_$1"
  cp "./swagger/api-$1.yaml" "./swagger/$1/api.yaml"
  cp -r ./dist/* "./swagger/$1"
}

function replace () {
  echo "... replacing swagger client package name and gateway"
  find "./swagger/api/api_$2" -type f -exec sed -i -e "s/swagger_client/api_$2/g" {} \;
  find "./swagger/api/api_$2" -type f -exec sed -i -e "s/self.host = .*/self.host = \"http:\/\/localhost:9095\"/g" {} \;
}

function move () {
  echo "... moving swagger static files"
  mkdir -p ./site/swagger
  cp -r "./swagger/$1" ./site/swagger
}

for key in "${!services[@]}"; do
  echo "Generating ${services[$key]} API"
  if [[ ! -z "${UPDATE}" ]]; then
    retrieve "$key" "${services[$key]}"
  fi
  generate "${services[$key]}"
  remove "${services[$key]}"
  copy "${services[$key]}"
  replace "$key" "${services[$key]}"
  move "${services[$key]}"
done

echo ""
echo "=============================="
echo " SUMMARY:"
echo "=============================="
echo ""
if [[ ! -z "${UPDATE}" ]]; then
  echo "- Retrieved ${#services[@]} updated Swagger endpoint defintions"
fi
echo "- Removed ${#services[@]} old Python APIs"
echo "- Generated ${#services[@]} new Python APIs"
echo "- Generated ${#services[@]} static Swagger Docs"
