from api_authentication.api.authentication_endpoint_api import AuthenticationEndpointApi
from api_container.api.container_endpoint_api import ContainerEndpointApi
from api_database.api.database_endpoint_api import DatabaseEndpointApi
from api_query.api.query_endpoint_api import QueryEndpointApi
from api_table.api.table_endpoint_api import TableEndpointApi


class RestClient:

    def __init__(self, hostname: str, username: str, password: str):
        self.token = None
        self.hostname = hostname
        self.username = username
        self.password = password
        self.authentication = AuthenticationEndpointApi()
        self.authentication.api_client.configuration.host = self.hostname
        self.container = ContainerEndpointApi()
        self.container.api_client.configuration.host = self.hostname
        self.database = DatabaseEndpointApi()
        self.database.api_client.configuration.host = self.hostname
        self.table = TableEndpointApi()
        self.table.api_client.configuration.host = self.hostname
        self.query = QueryEndpointApi()
        self.query.api_client.configuration.host = self.hostname

    def login(self) -> None:
        """
        Authenticates the user with provided credentials from the constructor. Also sets the authentication for all
        known services.
        """
        response = self.authentication.authenticate_user1({
            "username": self.username,
            "password": self.password
        })
        print("Authenticated user with id {}".format(response.id))
        self.token = response.token
        self.container.api_client.default_headers = {"Authorization": "Bearer " + self.token}
        self.database.api_client.default_headers = {"Authorization": "Bearer " + self.token}
        self.table.api_client.default_headers = {"Authorization": "Bearer " + self.token}
        self.query.api_client.default_headers = {"Authorization": "Bearer " + self.token}

    def find_database(self, container_id: int, database_id: int):
        """
        Finds a database in DBRepo via the provided container id and database id.
        :param: container_id: The container id.
        :param: database_id: The database id.
        :return: The database if successful. Exception otherwise.
        """
        response = self.database.find_by_id(container_id, database_id)
        print("Found database with id {}".format(response.id))
        return response

    def create_table(self, container_id: int, database_id: int, name: str, cols: [], col_types: []):
        """
        Creates a table in the provided container id and database id.
        :param: container_id: The container id.
        :param: database_id: The database id.
        :param: name: The table name.
        :return: The created container, if successful. Exception otherwise.
        """
        columns = [{
            "name": n,
            "type": t,
            "null_allowed": True,
            "primary_key": False,
            "unique": False
        } for n, t in zip(cols, col_types)]
        # send request
        response = self.table.create(body={
            "name": name,
            "description": "This table provides real-time air-data from around 170 air measuring stations in Austria",
            "columns": columns
        }, authorization="Bearer " + self.token, id=container_id, database_id=database_id)
        print("Created table with id {}".format(response.id))

    def find_tables(self, container_id: int, database_id: int) -> []:
        """
        Finds all tables in a container id and database id
        :param: container_id: The container id.
        :param: database_id: The database id
        :return: The list of tables
        """
        response = self.table.find_all(container_id, database_id)
        print("Found {} tables".format(len(response)))
        return response

    def find_table(self, container_id: int, database_id: int, table_id: int) -> {}:
        """
        Find a table in a container id and database id and table id
        :param: container_id: The container id.
        :param: database_id: The database id
        :param: table_id: The table id
        :return: The table, if successful. Exception otherwise.
        """
        response = self.table.find_by_id(container_id, database_id, table_id)
        print("Found table with id {}".format(table_id))
        return response
