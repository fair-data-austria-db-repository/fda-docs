from api_broker.BrokerServiceClient import BrokerServiceClient


class AmqpClient:

    def __init__(self, hostname: str, port: int, username: str, password: str):
        self.hostname = hostname
        self.port = port
        self.username = username
        self.password = password

    def send(self, exchange: str, routing_key: str, payload):
        broker = BrokerServiceClient(exchange=exchange, routing_key=routing_key, host=self.hostname,
                                     username=self.username, password=self.password)
        response = broker.send(payload)
        print("sent tuple to exchange {} with routing key {}".format(exchange, routing_key))
        return response
