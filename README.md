# DBRepo Docs

## Documentation

Versioning provider is `mike`. The idea is to have a documentation per Docker Image tage also in mkdocs, e.g. `latest` 
version for dev has a corresponding tag in mkdocs.

### Releases

* `1.3` -> master branch for Version 1.3
* `latest` -> always the `dev` branch

### Release version

Release e.g. `latest` to `1.4` when a merge from `dev` to `master` occurs:

    mike deploy --push latest
    mike retitle latest 1.4
    mike deploy --push --update-aliases 1.4