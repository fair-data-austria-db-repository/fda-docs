---
author: Martin Weise
hide:
- navigation
---

# Publications

## Refereed

##### 2022

1. Ekaputra, F. E., Weise, M., Flicker, K., Salleh, M. R., Rahman, N. A., Miksa, T., & Rauber, A. (2022). Towards A 
   Data Repository for Educational Factories. *Proceedings of the 8th International Conference on Data and Software
   Engineering*, pp. 149-154. DOI: [10.1109/ICoDSE56892.2022.9971958](https://doi.org/10.1109/ICoDSE56892.2022.9971958).

2. Weise, M., Staudinger, M., Michlits, C., Gergely, E., Stytsenko, K., Ganguly, R., & Rauber, A. (2022).
   DBRepo: a Semantic Digital Repository for Relational Databases. *International Journal of Digital Curation*,
   17(1), 11. DOI: [10.2218/ijdc.v17i1.825](https://doi.org/10.2218/ijdc.v17i1.825)

##### 2021

1. Weise, M., Michlits, C., Staudinger, M., Gergely, E., Stytsenko, K., Ganguly, R. and Rauber A., 2021. FDA-DBRepo: A
   Data Preservation Repository Supporting FAIR Principles, Data Versioning and Reproducible Queries. *Proceedings of
   the 17th International Conference on Digital Preservation*, Beijing, China, p.34.
   DOI: [10.17605/OSF.IO/B7NX5](http://doi.org/10.17605/OSF.IO/B7NX5)

## Other

1. Weise, M. (2023). A Repository and Compute Environment for Sensitive Data. FAIRness for Closed Data,
   at *EMBL Bioimaging and the European Open Science Cloud*, (Heidelberg, Germany). April, 19-20th, 2023.
   DOI: [10.34726/3946](https://doi.org/10.34726/3946)

2. Staudinger, M. (2022). DBRepo: A Repository to Save Research Databases. [Online].
   URL: [https://www.tuwien.at/en/tu-wien/news/news-articles/news/dbrepo](https://www.tuwien.at/en/tu-wien/news/news-articles/news/dbrepo)
   accessed 2022-04-12

2. Gergely, E. (2021). Better Support for Research: Current Cooperation Projects. [Online].
   URL: [https://zid.univie.ac.at/it-news/artikel/news/cluster-forschungsdaten/](https://zid.univie.ac.at/it-news/artikel/news/cluster-forschungsdaten/)
   accessed 2022-04-12
