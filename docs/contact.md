---
author: Martin Weise
hide:
- navigation
---

# Contact

## Team

### Project Management

Ao.univ.Prof. Dr. [Andreas Rauber](https://www.ifs.tuwien.ac.at/~andi)<br />
Technische Universit&auml;t Wien<br />
Research Unit Data Science<br />
Favoritenstra&szlig;e 9-11<br />
A-1040 Vienna, Austria

### Development

Projektass. Dipl.-Ing. [Martin Weise](https://ec.tuwien.ac.at/~weise/)<br />
Technische Universit&auml;t Wien<br />
Research Unit Data Science<br />
Favoritenstra&szlig;e 9-11<br />
A-1040 Vienna, Austria

## Contributors (alphabetically)

- Ganguly, Raman
- Gergely, Eva
- Grantner, Tobias
- Karnbach, Geoffrey
- Michlits, Cornelia
- Rauber, Andreas
- Staudinger, Moritz
- Stytsenko, Kirill
- Taha, Josef
- Tsepelakis, Sotiris
- Weise, Martin
