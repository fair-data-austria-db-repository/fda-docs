---
title: Home
author: Martin Weise
hide:
- navigation
---

# DBRepo: A Database Repository to Support Research

## Problem Statement

Digital repositories see themselves more frequently encountered with the problem of making databases accessible in their
collection. Challenges revolve around organizing, searching and retrieving content stored within databases and
constitute a major technical burden as their internal representation greatly differs from static documents most digital
repositories are designed for.

[Get Started](/infrastructures/dbrepo/1.3/get-started){ .action-button .md-button .md-button--primary }
[Learn More](/infrastructures/dbrepo/system){ .action-button .md-button .md-button--secondary }

## Application Areas

We present a database repository system that allows researchers to ingest data into a central, versioned repository
through common interfaces, provides efficient access to arbitrary subsets of data even when the underlying data store is
evolving, allows reproducing of query results and supports findable-, accessible-, interoperable- and reusable data.

## Releases

| Release | Links                                                                                                                                       | Status           |
|---------|---------------------------------------------------------------------------------------------------------------------------------------------|------------------|
| 1.3     | [Docs](/infrastructures/dbrepo/1.3/) &boxv; [Source](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services/-/tree/master) | Release Candidate |
| latest  | [Docs](/infrastructures/dbrepo/latest/) &boxv; [Source](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services/-/tree/dev) | Developer Preview |

Non-maintained releases:

| Release | Links                                                                                                     | Status |
|---------|-----------------------------------------------------------------------------------------------------------|--------|
| 1.2     | [Source](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services/-/releases/v1.1.1-alpha) | EOL    |
| 1.1     | [Source](https://gitlab.phaidra.org/fair-data-austria-db-repository/fda-services/-/tags/v1.1.0-alpha)     | EOL    |

## More Information

- Demonstration instance [https://dbrepo1.ec.tuwien.ac.at](https://dbrepo1.ec.tuwien.ac.at)
- Sandbox instance [https://dbrepo2.ec.tuwien.ac.at](https://dbrepo2.ec.tuwien.ac.at)
- System description [https://doi.org/10.2218/ijdc.v17i1.825](https://doi.org/10.2218/ijdc.v17i1.825)
